package whatjson

import (
	"bufio"
	"bytes"
	"strings"
)

// JSONType is a custom string type for json return types.
type JSONType string

const (
	// JSONArray denotes a json array type.
	JSONArray JSONType = "array"

	// JSONObject denotes a json object type.
	JSONObject JSONType = "object"

	// JSONUnknown denotes an unknown json type.
	JSONUnknown JSONType = "unknown"
)

// WhatJSON will check for the next non whitespace char after prefix match to confirm a JSON object or array.
func WhatJSON(b []byte, prefix ...string) (JSONType, error) {
	r := bufio.NewReader(bytes.NewReader(b))
	defer r.UnreadByte()

	pfx := ""
	if len(prefix) > 0 {
		pfx = prefix[0]
	}

	prefixMatch := false
	matchBuf := bytes.NewBufferString("")

	// Strip all whitespace from the prefix string.
	pfx = strings.Join(strings.Fields(pfx), "")
	if pfx == "" {
		prefixMatch = true
	}

	for {
		c, err := r.ReadByte()
		if err != nil {
			return JSONUnknown, err
		}

		// If we do not have a matched prefix, check the byte value and add to the buffer.
		if !prefixMatch {
			err := matchBuf.WriteByte(c)
			if err == nil && matchBuf.String() == pfx {
				prefixMatch = true
			}

			continue
		}

		// We have a matched prefix so check the next byte for json type.
		switch c {
		case '[':
			return JSONArray, nil

		case '{':
			return JSONObject, nil

		case ' ', '\t', '\r', '\n':
		// Deliberate fallthrough for whitespace

		default:
			return JSONUnknown, nil
		}
	}
}
