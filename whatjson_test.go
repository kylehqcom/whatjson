package whatjson

import (
	"testing"

	"io"
)

func TestWhatJSONArray(t *testing.T) {
	body := []byte("{data:{[[{")
	rt, err := WhatJSON(body, "{data:{")
	if err != nil {
		t.Error(err)
	}
	if JSONArray != rt {
		t.Error("Expected an array.")
	}

	// Check prefix with spaces expecting array
	rt, err = WhatJSON(body, "{data:{")
	if err != nil {
		t.Error(err)
	}
	if JSONArray != rt {
		t.Error("Expected an array.")
	}
}

func TestWhatJSONNoPrefix(t *testing.T) {
	body := []byte(" {data:{[[{")
	rt, err := WhatJSON(body)
	if err != nil {
		t.Error(err)
	}

	if JSONObject != rt {
		t.Error("Expected an object for no prefix.")
	}
}

func TestWhatJSONBadPrefix(t *testing.T) {
	body := []byte("")
	rt, err := WhatJSON(body, "bad")
	if err == nil {
		t.Error("Error should be set.")
	}
	if JSONUnknown != rt {
		t.Error("Expected an unknown object for bad prefix.")
	}
}

func TestWhatJSONDeepNesting(t *testing.T) {
	body := []byte("{data:{[[{")

	rt, err := WhatJSON(body, "{data:{[")
	if err != nil {
		t.Error(err)
	}
	if JSONArray != rt {
		t.Error("Expected an array.")
	}

	rt, err = WhatJSON(body, "{data:{[[")
	if err != nil {
		t.Error(err)
	}
	if JSONObject != rt {
		t.Error("Expected an array.")
	}

	rt, err = WhatJSON(body, "{data:{[[{")
	if err != io.EOF {
		t.Error(err)
	}
	if JSONUnknown != rt {
		t.Error("Expected an unknown.")
	}
}

func TestWhatJSONEmptyBody(t *testing.T) {
	body := []byte("")
	rt, err := WhatJSON(body, "")
	if err != io.EOF {
		t.Error("Expected EOF error")
	}
	if JSONUnknown != rt {
		t.Error("Expected an unknown object on empty body.")
	}
}

func TestWhatJSONNonJSONBody(t *testing.T) {
	body := []byte("nope")
	rt, err := WhatJSON(body, "")
	if err != nil {
		t.Error(err)
	}
	if JSONUnknown != rt {
		t.Error("Expected an unknown object on bad body.")
	}
}

func TestWhatJSONObject(t *testing.T) {
	// Assert empty prefix.
	body := []byte("{data:{[[{")
	rt, err := WhatJSON(body, "")
	if err != nil {
		t.Error(err)
	}
	if JSONObject != rt {
		t.Error("Expected an object as first char of body.")
	}

	// Check prefix expecting object
	rt, err = WhatJSON(body, "{data:")
	if err != nil {
		t.Error(err)
	}
	if JSONObject != rt {
		t.Error("Expected an object.")
	}

	// Check prefix with spaces expecting object
	rt, err = WhatJSON(body, "  {   data   :  ")
	if err != nil {
		t.Error(err)
	}
	if JSONObject != rt {
		t.Error("Expected an object.")
	}
}

func TestWhatJSONUnknown(t *testing.T) {
	body := []byte("{data:{[[{")
	rt, err := WhatJSON(body, "{")
	if err != nil {
		t.Error(err)
	}
	if JSONUnknown != rt {
		t.Error("Expected an unknown object.")
	}
}
