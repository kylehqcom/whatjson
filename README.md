# WhatJSON?
___
[![Build Status](https://gitlab.com/kylehqcom/whatjson/badges/master/build.svg)](https://gitlab.com/kylehqcom/whatjson/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kylehqcom/whatjson)](https://goreportcard.com/report/gitlab.com/kylehqcom/whatjson)

A simple util written in Go that parses a request body (of type []bytes) and returns whether the body is a JSON Object or Array type. Handy when consuming JSON api responses and you're not sure whether the resource returns a collection/array or a single record/object. Will return `unknown` if unable to match.

If you like this package or are using for your own needs, then let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)

If the resource has followed the [http://jsonapi.org/format/](http://jsonapi.org/format/) spec, then a single entity should be an object, a collection an array. [Reference](http://jsonapi.org/format/#document-top-level)

```
Primary data MUST be either:

* a single resource object, a single resource identifier object, or null, for requests that target single resources
  
* an array of resource objects, an array of resource identifier objects, or an empty array for requests that target resource collections
 
```

Feel free to import using `import "gitlab.com/kylehqcom/whatjson"` but you should probably just copy paste the code to remove dependencies.

The code works by checking for the next non whitespace character after an optional user supplied string prefix to confirm a JSON object or array.

## Example
Given this api response fragment

```
{
	"data": [{
		"id": "xyz",
		"name": "Foo"
	}]
}
```

Calling `WhatJSON()` with a prefix of `{"data":` will return an **Array** due to the next non whitespace character being a **[**

Calling `WhatJSON()` with a prefix of `{"data":[` will return an **Object** due to the next non whitespace character being a **{**

Calling `WhatJSON()` with an no prefix will return an **Object** due to the first non whitespace character being a **{**, the outer object to data.

Calling `WhatJSON()` with a prefix of `{"da` will return an **Unknown** due to the first non whitespace character being the letter **t** in daTa.

Note that whitespace is trimmed/removed from your prefix so `{ "d a t a" : [` is treated identically to `{"data":[`
